/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SRC_RILDOS_H_
#define SRC_RILDOS_H_

#include <stdint.h>
#include <stdlib.h>

typedef enum {
	RILDOS_MOD_BPSK,
	RILDOS_MOD_QPSK
} rildos_mod_t;

void
rildos_init(uint8_t *out, uint32_t seed);

uint32_t
rildos_create_frame(uint8_t *out, uint32_t svn, uint32_t uptime_secs);

int
rildos_spread(uint8_t *out, const uint8_t *in, const uint8_t *spread_seq,
              size_t len, rildos_mod_t mod);

#endif /* SRC_RILDOS_H_ */
